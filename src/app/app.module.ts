import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SecondComponent } from './components/second/second.component';
import { FirstComponent } from './components/first/first.component';
import { ComponentOneComponent } from './components/component-one/component-one.component';
import { ThirdComponent } from './components/third/third.component';
import { FourthComponent } from './components/fourth/fourth.component';
// jeżeli chcemy używać w aplikacji Angularowej 
// two-way-data-binding musimy zaimportować ForsmModule
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { TodoModule } from './modules/todo/todo.module';
import { ReactiveFormsComponent } from './components/reactive-forms/reactive-forms.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginFormComponent } from './components/login-form/login-form.component';

@NgModule({
  declarations: [
    // jeżeli tworzymy komponent z ręki to pamietajmy aby dodać go do tablicy declarations
    AppComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    FourthComponent,
    ComponentOneComponent,
    NavComponent,
    HomeComponent,
    PortfolioComponent,
    ReactiveFormsComponent,
    LoginFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    // 1. Krok
    // jeżeli chcemy używać nowy moduł w aplikacji to musimy
    // go zaimportować w tablicy imports
    TodoModule,
    ReactiveFormsModule,
    //moduł do wysyłania zapytań do API
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
