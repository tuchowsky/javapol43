import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // dataArr: string[] = [];
  dataArr: Array<string> = [
    'Katarzyna',
    'Krzysztof',
    'Michał'
  ];

  myImg: string = 'https://images.pexels.com/photos/169573/pexels-photo-169573.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260';

  isElementVisible: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  onButtonClick() {
    this.isElementVisible = !this.isElementVisible;
  }

}
