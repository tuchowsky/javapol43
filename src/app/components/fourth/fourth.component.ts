import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fourth',
// możemy pisać markup HTML bezpośrednio w komponencie, ale pamiętamy o tym, aby użyć właściwości
// template zamiast templateURL
// template: '<h1>Hello fourth component<h1>',
    templateUrl: './fourth.component.html',
    styles: ['h1 { color: red }']
})

// jeżeli tworzymy komponent z ręki to pamiętajmy aby dodać jego import oraz dodać go do tablicy declarations
// w pliku app.module.ts
export class FourthComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
