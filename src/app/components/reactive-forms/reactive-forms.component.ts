import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.scss']
})
export class ReactiveFormsComponent implements OnInit {
  searchForm: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      'search': new FormControl(null, Validators.required)
    })
  }

  onSubmit(): void {
    console.log(this.searchForm.value);

    if(!this.searchForm.valid) {
      this.searchForm.get('search').markAsTouched();
    } else {
      this.searchForm.reset();
    }
  }

}
