import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, OnDestroy {
  // Formularze
  //W angularze są dawa podejścia do tworzenia formularzy
  //Template driven
  //Reactive forms

  //aby korzystać z Reactive forms
  //1.importujemy RF do naszego app.module.ts
  //2.tworzenie zmiennej, która będzie trzymać nasz formularz

  loginForm: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.loginForm = new FormGroup(
      {
        'username': new FormControl(null, Validators.required),
        'password': new FormControl(null, Validators.required),
        'age': new FormControl(null, Validators.required)
      }
    )
  }

  onFormSubmit() {
    console.log(this.loginForm.value);
    console.log(this.loginForm.valid, 'czy formularz jest poprawnie wypełniony');
    
    // gdy dany formularz jest wypełnony poprawnie
    if(this.loginForm.valid) {
      this.loginForm.reset();
    }
  }

  ngOnDestroy(): void {

  }

}
