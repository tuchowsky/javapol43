import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  usersData: any = {};


  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getData();

    // podpięcie pod zmienną, która przetrzymuje zwrotkę z API
    this.apiService.apiData.subscribe(
      data => {
        this.usersData = data;
        console.log(this.usersData, 'this is data');
      }
    )
  }

}
