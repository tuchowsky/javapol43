import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss']
})

export class FirstComponent {
  myName: string = 'Artur';
  myAge: number = 34;
  isOld: boolean = false;

  inputValue: string = 'my value';

  myImg: string = 'https://images.pexels.com/photos/169573/pexels-photo-169573.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260';


  constructor() { }

  onButtonClick() {
    this.myAge = 18;
  }

  onButtonEnter() {
    this.myAge = 34;
  }

  onSubmit() {
    console.log(this.inputValue);
  }



}
