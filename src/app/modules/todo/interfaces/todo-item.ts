export interface TodoItemInteface {
    id: number;
    description: string;
    date: Date;
    isDone: boolean;
}
