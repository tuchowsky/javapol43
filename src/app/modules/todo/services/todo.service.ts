import { Injectable } from '@angular/core';
import { TodoItemInteface } from '../interfaces/todo-item';

@Injectable({
  providedIn: 'root'
})

export class TodoService {

  todoItemsArray: Array<TodoItemInteface> = [
    {
      id: 0,
      description: 'Todo 1',
      date: new Date(),
      isDone: false
    },
    {
      id: 1,
      description: 'Todo 2',
      date: new Date(),
      isDone: false
    },
    {
      id: 2,
      description: 'Todo 3',
      date: new Date(),
      isDone: true
    },
    {
      id: 3,
      description: 'Todo 4',
      date: new Date(),
      isDone: false
    }
  ];

  constructor() { }

  deleteItem(indexElementu: number) {
    // this.todoItemsArray.splice('index od którego chcemy usuwać', 'ilość elementów które chcemy usunąć')
    this.todoItemsArray.splice(indexElementu, 1);
  }

}
