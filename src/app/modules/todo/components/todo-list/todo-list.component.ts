import { Component, Input, OnInit } from '@angular/core';
import { TodoItemInteface } from '../../interfaces/todo-item';
import { TodoService } from '../../services/todo.service';


@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
 
  todoItems: Array<TodoItemInteface> = [];

  // todoItems: Array<string> = [
  //   'Dominik',
  //   'Katarzyna',
  //   'Krzysztof',
  //   'Michał'
  // ];

  // username: string = 'My name';

  constructor(private todoService: TodoService) { 
    this.todoItems = this.todoService.todoItemsArray;
  }

  ngOnInit(): void {
  }

  onDeleteTodo(index: number) {
    console.log(index);
    this.todoService.deleteItem(index);

    // console.log('event został wywołany na dziecku, a metoda wywołała się z komponentu rodzica');
  }

}
