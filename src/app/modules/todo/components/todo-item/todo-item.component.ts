import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

import { TodoItemInteface } from '../../interfaces/todo-item';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})

export class TodoItemComponent implements OnInit {
  @Input() data: TodoItemInteface;
  @Output() deleteItem = new EventEmitter<Event>();
  //1. Definica Output()
  //2. Stworzenie metody onButtonClick i przekazanie do niej eventu
  //3. Wywołanie metody onButtonClick w templatce html (todo-item.component.html)
  //4. Przy wyświetlaniu komponentu <app-todo-item> definiujemy nasz nowo 
  // stworzony event i przypisujemy do nowej metody onDeteteTodo()
  //5. w pliku todo-list.component.ts tworzymy nową metodę onDeleteTodo()
  //6. w pliku z serwisem todo.service.ts tworzymy nową metodę deleteItem()
  // która będzie odpowiadała za usuwanie elementów z tablicy
  // dane mogą pochodzić z bazy danych
  // w naszym przypadku jest to tablica z definiowanymi na sztywko wartościami
  //7. wracamay do pliku todo-list.comopnent.ts i wywołujemy metodę z serwisu
  // jeżeli użytkownik kliknie butto Delete
  constructor() { }

  ngOnInit(): void {
  }

  onButtonClick(cokolwiek: Event) {
    // alert('działa');
    // console.log(cokolwiek);
    this.deleteItem.emit(cokolwiek);
  }

}
