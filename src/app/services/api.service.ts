import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
//do poczytania rxjs

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL: string = 'https://reqres.in/api/users';

  apiData = new BehaviorSubject({});

  constructor(private http: HttpClient) { }

  getData() {
    this.http.get(this.apiURL).subscribe(
      data => { 
        // console.log(data);
        this.apiData.next(data);
      }
    );
  }

}
